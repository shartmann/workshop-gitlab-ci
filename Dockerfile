FROM ruby:2.5-alpine
MAINTAINER Serge Hartmann <serge.hartmann@gmail.com>
ADD ./ /app/
WORKDIR /app
USER root
ENV PORT 5000
ENV PASSWORD PafLeChien
EXPOSE 5000
CMD ["sh", "-c", "while :; do ruby ./server.rb; done"]
